<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';NoQnr7a~HM<Jmx!3HavcQb!?li_Q1`2DE4]&0[EB[Ir?bZKRcf[%^S;3RFr6M0H');
define('SECURE_AUTH_KEY',  ':@594/TpC6-qNK=-,6A>1*~qm,+=jURza~bhmw)r;su^K*/IfFTD$Q$px-.4rAQ<');
define('LOGGED_IN_KEY',    '$5h8yrcbHATvgMxnHR8VZXXr[bk/wzZtl5%:-ivY1;p&Hgcn32yMYDIXo*Q;0*6b');
define('NONCE_KEY',        'Yi,iy2MP2Bc8PNRF9,=FB6Qp.=6Q?#&TRU)x_1fg&2XQXgw-1Mqn[TWd4o~QsYL4');
define('AUTH_SALT',        '3. .bDTrqb!]_y=KDZRh`.<IZIb4GEwtw%&O[GC&]wOCHHOM& #UryG}}Q827<&v');
define('SECURE_AUTH_SALT', 'Zhj0Va`eYp1YDuPN}/MAha8w7jAQAoZ>3,91u~;2} bpA%^m-]q))b4ba1yWxez[');
define('LOGGED_IN_SALT',   'zfY 2wdJmb?XS]=~Qn`n%#6>PHE1:M*A!H54I`kI(2mL@%jtVYIl5EpPE9PZ|DF?');
define('NONCE_SALT',       'A<rACl!kUm~U<]CU>18)Vj:ZY9g*7rmuOpGmlK{[v fsRuj%d*>I<fQa[,|$kpJ8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
